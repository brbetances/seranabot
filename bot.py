# bot.py
import os
from twitchio.ext import commands

bot = commands.Bot(
    irc_token=os.environ['TMI_TOKEN'],
    client_id=os.environ['CLIENT_ID'],
    nick=os.environ['BOT_NICK'],
    prefix=os.environ['BOT_PREFIX'],
    initial_channels=[os.environ['CHANNEL']]
)

## COMMENT: events
@bot.event
async def event_ready():
    'called when the bot goes online.'
    print(f"{os.environ['BOT_NICK']} is online!")
    ws = bot._ws
    await ws.send_privmsg(os.environ['CHANNEL'], f"/me has landed!")

## COMMENT: commands
@bot.command(name='ping')
async def ping(ctx):
    await ctx.send('pong!')

@bot.command(name='today')
async def today(ctx, opt: str = ''):
    if opt != '':
        today.set_today(opt)
        await ctx.send("command today set.") ## FIX: make this friendlier 
    else:
        await ctx.send(today.get_today()) ## FIX: make this friendlier, too

## COMMENT: classes
class Today:
    def __init__(self, today = "what are we doing today?"):
        self._today = today
    def get_today(self):
        return self._today
    def set_today(self, x):
        self._today = x

today = Today()
## COMMENT: run the bot
if __name__ == "__main__":
    bot.run()
